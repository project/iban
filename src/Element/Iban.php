<?php

namespace Drupal\iban\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides an International Bank Account Number field form element.
 *
 * Properties:
 * - #maxlength: Maximum number of characters of input allowed.
 * - #size: The size of the input element in characters.
 * - #autocomplete_route_name: A route to be used as callback URL by the
 *   autocomplete JavaScript library.
 * - #autocomplete_route_parameters: An array of parameters to be used in
 *   conjunction with the route name.
 *
 * Usage example:
 * @code
 * $form['iban'] = array(
 *   '#type' => 'iban',
 *   '#title' => $this->t('Iban'),
 *   '#default_value' => $node->iban,
 *   '#size' => 60,
 *   '#maxlength' => 32,
 *   '#required' => TRUE,
 * );
 * @endcode
 *
 * @FormElement("iban")
 */
class Iban extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#maxlength' => 31,
      '#autocomplete_route_name' => FALSE,
      '#process' => [
        [$class, 'processAutocomplete'],
        [$class, 'processAjaxForm'],
        [$class, 'processPattern'],
        [$class, 'processGroup'],
      ],
      '#element_validate' => [
        [$class, 'validateIban'],
      ],
      '#pre_render' => [
        [$class, 'preRenderIban'],
        [$class, 'preRenderGroup'],
      ],
      '#theme' => 'input__iban',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      // This should be a string, but allow other scalars since they might be
      // valid input in programmatic form submissions.
      if (!is_scalar($input)) {
        $input = '';
      }
      return str_replace(["\r", "\n"], '', $input);
    }
    return NULL;
  }

  /**
   * Form element validation handler for #type 'iban'.
   *
   * Note that #maxlength and #required is validated by _form_validate() already.
   */
  public static function validateIban(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = trim($element['#value']);
    $form_state->setValueForElement($element, $value);

    if ($value !== '' && !\IsoCodes\Iban::validate($value)) {
      $form_state->setError($element, t('The international bank account number %iban is not valid.', ['%iban' => $value]));
    }
  }

  /**
   * Prepares a #type 'textfield' render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderIban($element) {
    $element['#attributes']['type'] = 'iban';
    Element::setAttributes($element, ['id', 'name', 'value', 'size', 'maxlength', 'placeholder']);
    static::setAttributes($element, ['form-iban']);

    return $element;
  }

}
